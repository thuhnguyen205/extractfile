﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
namespace Module2Project
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void miQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void miOpen_Click(object sender, EventArgs e)
        {
            DialogResult drResult = ofdOpenFile.ShowDialog();

            if (drResult == DialogResult.OK)
            {
                FileStream fsOpenFile = File.Open(ofdOpenFile.FileName, FileMode.Open, FileAccess.Read);
                Archive(fsOpenFile);
                fsOpenFile.Close();
                btnExtract.Enabled = true;
            }
        }

        private void Archive(FileStream fsOpenFile)
        {
            fsOpenFile.Seek(-4, SeekOrigin.End);
            byte[] byBuffer = new byte[4];
            int iOffsetFile = fsOpenFile.Read(byBuffer, 0, 4);
            int iDict = byBuffer[0] + byBuffer[1] * 256 + byBuffer[2] * (65536) + byBuffer[3] * (16777216);
            fsOpenFile.Seek(-iDict, SeekOrigin.End);
            int iNumBytesDict = iDict - 4;
            int iNumBytesRead = 0;
            while (iNumBytesRead < iNumBytesDict)
            {
                int iFirstByte = fsOpenFile.ReadByte();
                byte[] byFileName = new byte[iFirstByte];
                fsOpenFile.Read(byFileName, 0, byFileName.Length);
                string strFileName = Encoding.ASCII.GetString(byFileName);
                fsOpenFile.Read(byBuffer, 0, byBuffer.Length);
                int iOffsetValue = byBuffer[0] + byBuffer[1] * 256 + byBuffer[2] * (65536) + byBuffer[3] * (16777216);
                fsOpenFile.Read(byBuffer, 0, byBuffer.Length);
                int iLengthValue = byBuffer[0] + byBuffer[1] * 256 + byBuffer[2] * (65536) + byBuffer[3] * (16777216);
                int iNewRowIndex = dgvContent.Rows.Add();
                DataGridViewRow dgvrNew = dgvContent.Rows[iNewRowIndex];
                dgvrNew.Cells["FileName"].Value = strFileName;
                dgvrNew.Cells["Length"].Value = iLengthValue;
                dgvrNew.Cells["Offset"].Value = iOffsetValue;
                iNumBytesRead += 1 + iFirstByte + 4 + 4;
            }
        }

        private void btnExtract_Click(object sender, EventArgs e)
        {
            DataGridViewRow dgvrRow = dgvContent.SelectedRows[0];
            sfdSaveFile.FileName = (string)dgvrRow.Cells["FileName"].Value;
            DialogResult drResult = sfdSaveFile.ShowDialog();
            if (drResult == DialogResult.OK)
            {
                FileStream fsReadFile = File.Open(ofdOpenFile.FileName, FileMode.Open, FileAccess.Read);
                FileStream fsWriteFile = File.Open(sfdSaveFile.FileName, FileMode.Create, FileAccess.Write);
                int iOffset = (int)dgvrRow.Cells["Offset"].Value;
                fsReadFile.Seek(iOffset, SeekOrigin.Begin);
                int iLength = (int)dgvrRow.Cells["Length"].Value;
                int iNumBytesRemain = iLength;
                while (iNumBytesRemain > 0)
                {
                    int iNumBytesRead = Math.Min(4096, iNumBytesRemain);
                    byte[] byBuffer = new byte[iNumBytesRead];
                    fsReadFile.Read(byBuffer, 0, byBuffer.Length);
                    fsWriteFile.Write(byBuffer, 0, iNumBytesRead);
                    iNumBytesRemain = iNumBytesRemain - iNumBytesRead;
                }
                fsReadFile.Close();
                fsWriteFile.Close();
            }
        }
    }
}
